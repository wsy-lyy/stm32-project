# STM32项目-开发软件keil

#### keil介绍
Keil公司是一家业界领先的微控制器（MCU）软件开发工具的独立供应商。Keil公司由两家私人公司联合运营，分别是德国慕尼黑的Keil Elektronik GmbH和美国德克萨斯的Keil Software Inc。Keil公司制造和销售种类广泛的开发工具，包括ANSI C编译器、宏汇编程序、调试器、连接器、库管理器、固件和实时操作系统核心（real-time kernel）。有超过10万名微控制器开发人员在使用这种得到业界认可的解决方案。其Keil C51编译器自1988年引入市场以来成为事实上的行业标准，并支持超过500种8051变种。
Keil公司在2005年被ARM公司收购。

与ARM编辑
Keil公司2005年由ARM公司收购。其两家公司分别更名为ARM Germany GmbH和ARM Inc。Keil公司首席执行官Reinhard Keil表示：“作为ARM Connected Community中的一员，Keil和ARM保持着长期的良好关系。通过这次收购，我们将能更好地向高速发展的32位微控制器市场提供完整的解决方案，同时继续在μVision环境下支持我们的8051和C16x编译器。”
而后ARM Keil推出基于μVision界面，用于调试ARM7，ARM9，Cortex-M内核的MDK-ARM开发工具，用于为控制领域的开发。

##### Keil μVision2
KeilμVision2是美国Keil Software公司出品的51系列兼容单片机C语言软件开发系统，使用接近于传统C语言的语法来开发，与汇编相比，C语言易学易用,而且大大的提高了工作效率和项目开发周期,他还能嵌入汇编，您可以在关键的位置嵌入，使程序达到接近于汇编的工作效率。Keil C51标准C编译器为8051微控制器的软件开发提供了C语言环境,同时保留了汇编代码高效,快速的特点。C51编译器的功能不断增强，使你可以更加贴近CPU本身，及其它的衍生产品。C51已被完全集成到μVision2的集成开发环境中，这个集成开发环境包含：编译器，汇编器，实时操作系统，项目管理器，调试器。μVision2 IDE可为它们提供单一而灵活的开发环境。

##### Keil μVision3
2006年1月30日ARM推出全新的针对各种嵌入式处理器的软件开发工具，集成Keil μVision3的RealView MDK开发环境。RealView MDK开发工具Keil μVision3源自Keil公司。RealView MDK集成了业内领先的技术，包括Keil μVision3集成开发环境与RealView编译器。支持ARM7、ARM9和最新的Cortex-M3核处理器，自动配置启动代码，集成Flash烧写模块，强大的Simulation设备模拟，性能分析等功能，与ARM之前的工具包ADS等相比，RealView编译器的最新版本可将性能改善超过20%。

##### Keil μVision4
2009年2月发布Keil μVision4，Keil μVision4引入灵活的窗口管理系统，使开发人员能够使用多台监视器，并提供了视觉上的表面对窗口位置的完全控制的任何地方。新的用户界面可以更好地利用屏幕空间和更有效地组织多个窗口，提供一个整洁，高效的环境来开发应用程序。新版本支持更多最新的ARM芯片，还添加了一些其他新功能。
2011年3月ARM公司发布最新集成开发环境RealView MDK开发工具中集成了最新版本的Keil μVision4，其编译器、调试工具实现与ARM器件的最完美匹配。


##### Keil μVision5
2013年10月，Keil正式发布了Keil μVision5 IDE。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0223/124059_87af992b_8645630.png "屏幕截图.png")

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
