# STM32项目

#### 介绍
STM32项目


#### PCB制作网站 EDA
[立创EDA](https://www.lceda.cn/)


#### 项目列表

[1.基于STM32大棚温湿度检测蓝牙APP控制](https://gitee.com/wsy-lyy/stm32-greenhouse-temperature-and-humidity-detection-bluetooth-app-control-design)

基于STM32单片机大棚温湿度检测无线蓝牙APP控制设计，本设计电路由STM32单片机+风扇控制电路+温湿度传感器电路+OLED液晶显示电路+蓝牙模块电路+电源电路组成。


[2.基于STM32单片机智能RFID刷卡汽车位锁桩设计](https://gitee.com/wsy-lyy/RFID)

本设计由STM32F103C8T6单片机核心板电路+LCD1602液晶显示电路+RFID模块电路+按键电路+继电器电路组成。

[3.基于STM32智能电表WIFI插座APP交流电压电流检测](https://gitee.com/wsy-lyy/STM32_WIFI_Socket_App_Detect)

本设计由STM32电路+交流电压电流检测模块电路+WIFI模块电路+指示灯电路组成,通过电压互感器TV1005M和电流互感器TA1005M分别检测交流电压和交流电流值，手机APP和WiFi模块互联后，可以实时显示交流电压、交流电流、功率和电量实时显示在手机上,当功率超过200W时，继电器自动断开。功率不超过200W时，可以手动控制继电器的开关,手机和WiFi模块连接后，手机上显示计时时间。

[4.基于STM32智能手环脉搏心率计步器体温示设计](https://gitee.com/wsy-lyy/STM32-Intelligent-Hands)

本设计由STM32F103C8T6单片机核心板电路+ADXL345传感器电路+心率传感器电路+温度传感器+lcd1602电路组成。通过重力加速度传感器ADXL345检测人的状态，计算出走路步数、走路距离和平均速度。通过心率传感器实时检测心率，通过温度传感器检测温度。oled实时显示步数、距离和平均速度、心率以及温度值。

[5.STM32实现的温度/心率/步数设计基](https://gitee.com/wsy-lyy/STM32_Temperature_heart)

以STM32为控制，通过重力加速度传感器ADXL345检测人的状态，计算出走路步数、走路距离和平均速度。通过心率传感器实时检测心率，通过温度传感器检测温度，OLED实时显示步数、距离和平均速度、心率以及温度值。

[6、基于STM32单片机物联网智能家居项目设计](https://gitee.com/wsy-lyy/STM32_HOME)

基于STM32单片机完成温度、湿度、光照、PM2.5、风速等监测，并通过MCU处理之后上报至云端。软件系统手机通过小程序，可以实时显示测试数据，并可以远程控制硬件系统动作，比如继电器的开关。

[7、基于STM32单片机的智能小区环境监测设计](https://gitee.com/wsy-lyy/STM32_Community)

本设计应用STM32F103单片机最小系统。配以输入输出部分，通过采集温湿度、甲醛、PM2.5等数据在OLED液晶上显示，内加单独时钟晶振电路，保护断电后时间参数不变，外接5v电源对整个系统供电。

[8、基于STM32 单片机人群定位调速智能风扇设计](https://gitee.com/wsy-lyy/STM32_People)

本系统采用STM32单片机为控制器，分为主控台和工作区两部分。系统通过热释红外传感器定位人群信息，在主控台设置阈值温度、转速与温度的对应关系。通过STM32控制NRF24L01将信息发送至工作区，工作区通过STM32控制NRF24L01接收到信号，将信号传给8266，并将DS18B20温度传感器检测到的温度通过NRF24L01回传给主控台，ATmega16将接收到的信号进行处理，进而控制直流电机的转速和舵机的转角。

[9、基于STM32的二维码识别解码设计](https://gitee.com/wsy-lyy/STM32_CODE)

STM32的二维码识别解码设计

[10、基于STM32的数据采集心率检测仪](https://gitee.com/wsy-lyy/STM32_Heart_rate)

该设计本是以STM32为控制核心，利用芯片内部的模数转换器来采集外部的模拟信号，并在TFT液晶屏的配合下来显示采集的数据。为便于直观分析，还将采集的数据绘制成波形图。为验证其设计功能，特配置了心率传感器来获取心率信号，经实际验证能在采集过程中达到设计的基本要求，为波形的图像文件生成提供了相应的支持。此外，在外挂串行通信模块的条件下，能实现采集数据的上传以供更深层次的数据分析和处理。

软件架构

- 

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
